pipeline {
  agent any

  tools {
    jdk 'jdk8'
    maven 'maven3.6.2'
  }

  stages {

    stage('Verify Feature branch or Deploy main') {
      when {
        anyOf {
          branch 'main'
          branch 'feature/*'
        }
      }
      steps {
        script {
          configFileProvider([configFile(fileId: 'e374c569-a9a8-4d73-bfd8-f8429ef13150', variable: 'MAVEN_SETTINGS')]) {
            def branchAction = ''
            switch (env.BRANCH_NAME) {
            case 'main':
              branchAction = 'deploy'
              break
            case 'feature/*':
              branchAction = 'verify'
              break
            default:
              branchAction = 'verify'
            }
            sh 'mvn -s $MAVEN_SETTINGS compile'
            sh "mvn test"
            sh "mvn -s $MAVEN_SETTINGS $branchAction"

          }
        }
      }
    }


    stage('Calculate version artifact') {
      when {
        branch 'release/*'
      }
      steps {
        script {
          sshagent(['ff5d072a-613c-4863-a86c-2580a4b08b8c']) {
            def majorMinor = env.BRANCH_NAME.tokenize("/")[1]
            def gitTags = sh(script: "git tag -l --sort=-v:refname '${majorMinor}.*'", returnStdout: true).trim()
            def nextZ = 0

            if (gitTags) {
              def tagList = gitTags.tokenize("\n")
              if (tagList && !tagList.isEmpty()) {
                def lastVersion = tagList[0]
                nextZ = lastVersion.tokenize(".")[2].toInteger() + 1
              }
            }

            env.FULL_VERSION = "${majorMinor}.${nextZ}"

          }
        }
      }
    }

    stage('Set current version') {
      when {
        branch 'release/*'
      }
      steps {
        script {
          sh "mvn versions:set -DnewVersion=${env.FULL_VERSION}"
        }
      }
    }
    

    stage('Compile') {
      when {
        branch 'release/*'
      }
      steps {
        script {
          configFileProvider([configFile(fileId: 'e374c569-a9a8-4d73-bfd8-f8429ef13150', variable: 'MAVEN_SETTINGS')]) {
            sh 'mvn -s $MAVEN_SETTINGS compile'
          }
        }
      }
    }

    stage('Unit Tests') {
      when {
        branch 'release/*'
      }
      steps {
        script {
          sh 'mvn test'
        }
      }
    }




    stage('Publish Artifact to Release repo') {
      when {
        branch 'release/*'
      }
      steps {
        script {
          configFileProvider([configFile(fileId: 'e374c569-a9a8-4d73-bfd8-f8429ef13150', variable: 'MAVEN_SETTINGS')]) {
            sh 'mvn -s $MAVEN_SETTINGS deploy'
          }
        }
      }
    }

    stage('Tag last release and push') {
      when {
        branch 'release/*'
      }
      steps {
        script {
          sshagent(['0c327000-553c-4fa7-a79f-7c79dca3a737']) {
            sh "git clean -n"
            sh "git tag ${env.FULL_VERSION}"
            sh "git push origin --tags"
          }
        }
      }
    }
  }
}